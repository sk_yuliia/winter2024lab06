public class LuckyCardGameApp{
	public static void main(String[] args){
		//Initialization of GameManager object 
		GameManager manager = new GameManager();
		int totalPoints = 0;
		System.out.println("~~Welcome to Lucky Card Game!~~");
		int roundsCount = 1;
		//Repeats as long as the drawPile has value more than 1, and totalPoints is less than 5
		while(manager.getNumberOfCards() > 1 && totalPoints < 5){
			System.out.println("-----------------------------" + "\n" + "Round: " + roundsCount);
			System.out.println(manager);
			totalPoints += manager.calculatePoints();
			System.out.println("_____________________________" + "\n" + "Total Points after round " + roundsCount + ": " + totalPoints);
			manager.dealCard();
			roundsCount++;
		}
		//Prints final score 
		System.out.println("~~~~~~~~~~~~~~~~~" + "\n" + "Final Score: " + totalPoints);
		//Prints messages stating the result of the game based on the score
		if (totalPoints < 5) {
            System.out.println("Sorry, you lose!");
		} else {
           System.out.println("Congratulations, you win!");
		}
	}
	
}
