public class GameManager{ 
	
	//Fields 
	private Deck drawPile;
	private Card centerCard; 
	private Card playerCard;
	
	//Constructor 
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.playerCard = drawPile.drawTopCard();
        this.centerCard = drawPile.drawTopCard();
	}
	
	//toString method 
	public String toString(){
		return "Center card: " + this.centerCard + "\n" + "Player card: " + this.playerCard;
	}
	
	//Updates the playerCard and centerCard to the two new cards
	public void dealCard(){
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard();
        this.centerCard = this.drawPile.drawTopCard();
	}
	
	//Returns an int representing the number of cards on the drawPile
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	//Compares centerCard and playerCard based on their suits and values 
	public int calculatePoints(){
		if(this.centerCard.getValue().equals(this.playerCard.getValue())){
			return 4;
		} else if (this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return 2;
		} else { 
			return -1;
		}
	
	}
	
	
}
